#!/bin/bash

cd /tmp

echo "Boot partition: "
read part_boot
echo "Data partition: "
read part_data
echo "Swap partition: "
read part_swap
echo "Hostname: "
read hostname

mkfs.fat -F 32 "${part_boot}"
mkfs.ext4 "${part_data}"
mkswap --verbose "${part_swap}"

mkdir /new_install
mount "${part_data}" /new_install
mount --mkdir "${part_boot}" /new_install/boot
swapon "${part_swap}"

pacstrap -K /new_install base linux linux-firmware
genfstab -U /new_install >> /new_install/etc/fstab

arch-chroot /new_install ln -sf /usr/share/zoneinfo/Region/City /etc/localtime
arch-chroot /new_install hwclock --systohc
arch-chroot /new_install sed -e "s/#cs_CZ.UTF-8 UTF-8/cs_CZ.UTF-8 UTF-8/g"
arch-chroot /new_install sed -e "s/#en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/g"
arch-chroot /new_install locale-gen
arch-chroot /new_install echo -n "LANG=en_US.UTF-8" > /etc/locale.conf
arch-chroot /new_install echo -n "KEYMAP=de-latin1" > /etc/vconsole.conf
arch-chroot /new_install echo -n "${hostname}" > /etc/hostname
arch-chroot /new_install mkinitcpio -P
echo "Create root password:"
arch-chroot /new_install passwd
arch-chroot /new_install pacman -Syu --noconfirm base-devel
arch-chroot /new_install pacman -S git grub efibootmgr
arch-chroot /new_install grub-install --target=x86_64-efi --efi-directory=esp --bootloader-id=GRUB
